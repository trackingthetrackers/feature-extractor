#!/bin/sh -e

pw=`./vault_password_file.sh`
printf $pw | gpg --sign --encrypt \
                 --recipient aaron@lo-res.org \
                 --recipient admin@f-droid.org \
                 --recipient bubu@bubu1.eu \
                 --recipient ciaran@ciarang.com \
                 --recipient hans@guardianproject.info \
                 --recipient michael@poehn.at \
                 --output vault_password
