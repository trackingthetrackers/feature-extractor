# feature extractor server

Running this ansible playbook will do the following things:
* create `fdroid` user
* update/install dependencies for running F-Droid buildserver
* update/install fdroidserver and fdroiddata to latest master

## System requirements

* Debian 10 (buster)
* amd64 based CPU with VMX support (for now only intel)
* min. 2TB disk space

Running the setup will download terabytes of data  on the first run. So a reasonably fast internet connection is required too.
## Deployment to remote server

You simply need to run the playbook from this project to bootstrap
to a remote host.

First you'll need to install ansible, on your local machine e.g.:

    sudo apt install ansible

Then install the bare minimum for _ansible_ on the remote host:

    sudo apt install python3 sudo

Fetch ansible dependencies:

    ansible-galaxy install -f -r requirements.yml

Then you can immediately start the deployment:

    ansible-playbook -i remoteHost, provision.yml

You'll need to replace `remoteHost` with the name of the remote host as
configured in your ssh config. (Tip: See `man 5 ssh_config` for help with
ssh config.) You'll also need to be root or have sudo privileges on the
remote server to successfully run this script.

